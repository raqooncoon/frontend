import { FC, ReactNode, RefObject, useEffect, useRef } from "react";
import { createPortal } from "react-dom";
import styles from "./ModalDropdown.module.scss";

const recalcPosition = (
  rt: HTMLDivElement | null,
  md: HTMLDivElement | null
) => {
  if (rt && md) {
    const rect = rt.getBoundingClientRect();
    const height = md.getBoundingClientRect().height;
    const cropLine = window.scrollY + window.innerHeight - 10;
    let top =
      window.scrollY + rect.top + height > cropLine
        ? cropLine - height
        : window.scrollY + rect.top;
    if (top < window.scrollY + 10) top = window.scrollY + 10;
    md.style.width = `${rect.width}px`;
    md.style.top = `${top}px`;
    md.style.left = `${rect.left}px`;
    md.style.maxHeight = `${window.innerHeight - 20}px`;
  }
};

const ModalDropdown: FC<{
  inpotRef: RefObject<HTMLDivElement>;
  inputRef: RefObject<HTMLInputElement>;
  children: ReactNode;
  open: boolean;
  onClose: () => void;
  resizeTrigger: unknown[];
}> = ({ inpotRef, inputRef, children, open, onClose, resizeTrigger }) => {
  const portalRoot = useRef(document.createElement("div"));
  const modalDropdownRef = useRef<HTMLDivElement>(null);
  const openRef = useRef(false);

  useEffect(() => {
    const memoPortalRoot = portalRoot.current;
    const rt = inpotRef.current;
    const md = modalDropdownRef.current;
    const isOpen = openRef;
    memoPortalRoot.style.display = "none";
    document.body.appendChild(memoPortalRoot);
    function updateSize() {
      if (rt && md && isOpen.current) {
        recalcPosition(rt, md);
      }
    }
    window.addEventListener("resize", updateSize);
    window.addEventListener("scroll", updateSize);
    updateSize();
    return () => {
      document.body.removeChild(memoPortalRoot);
      window.removeEventListener("resize", updateSize);
      window.removeEventListener("scroll", updateSize);
    };
  }, []);

  useEffect(() => {
    const memoPortalRoot = portalRoot.current;
    const rt = inpotRef.current;
    const md = modalDropdownRef.current;
    if (open) {
      memoPortalRoot.style.display = "block";
      recalcPosition(rt, md);
      setTimeout(() => {
        if (inputRef.current) {
          inputRef.current.focus();
        }
      }, 0);
    } else {
      memoPortalRoot.style.display = "none";
    }
    openRef.current = open;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, resizeTrigger]);

  return createPortal(
    <>
      <div className={styles.backdrop} onClick={onClose} />
      <div className={styles.modaldropdown} ref={modalDropdownRef}>
        {children}
      </div>
    </>,
    portalRoot.current
  );
};

export default ModalDropdown;
